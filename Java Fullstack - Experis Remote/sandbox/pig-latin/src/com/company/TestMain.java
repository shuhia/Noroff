package com.company;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestMain {
    Main main = new Main();
    @Test
    void testCaseOne(){
        var sentenceOne = "this is pig latin";
        var expected = "isthay isway igpay atinlay";
        var actual = main.pigLatinSentence(sentenceOne);
        assertEquals(expected, actual);
    }

    @Test
    void testCaseTwo(){
        var sentenceOne = "wall street journal";
        var expected = "allway eetstray ournaljay";
        var actual = main.pigLatinSentence(sentenceOne);
        assertEquals(expected, actual);
    }

    @Test
    void testCaseThree(){
        var sentenceOne = "raise the bridge";
        var expected = "aiseray ethay idgebray";
        var actual = main.pigLatinSentence(sentenceOne);
        assertEquals(expected, actual);
    }

    @Test
    void testCaseFour(){
        var sentenceOne = "all pigs oink";
        var expected = "allway igspay oinkway";
        var actual = main.pigLatinSentence(sentenceOne);
        assertEquals(expected, actual);
    }
}
