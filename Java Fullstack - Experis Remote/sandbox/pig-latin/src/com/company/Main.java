package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Main {

    public static void main(String[] args) {

    }

    public String pigLatinSentence(String sentence){
        String[] words = sentence.split(" ");
        StringBuilder pigLatinSentence = new StringBuilder();
        String[] newWords = convertWordsToPigLatin(words);
        return String.join(" ", newWords);

    }
    private String[] convertWordsToPigLatin(String[] word){
        String[] newWords = new String[word.length];
        for (int i = 0; i < word.length; i++) {
            newWords[i]= convertWordToPigLatin(word[i]);
        }
        return newWords;
    }
    private String convertWordToPigLatin(String word) {
        // Matches a vocal
        if(matchVocal(word.charAt(0))){
            return word + "way";
        }
        for(int i = 0; i< word.length(); i++){
            if(matchVocal(word.charAt(i))){
                // copy string from 0 to i. charAt(i) is excluded.
                String front = word.substring(0,i);
                // copy string from i to length of word.
                String back = word.substring(i);

                return back + front+"ay";
            }
        }


        return word;
    }
    private boolean matchVocal(char character){
        char[] vocals = "aeiou".toCharArray();
        for(var vocal : vocals){
            if(vocal == character){
                return true;
            }
        }
        return false;
    }
}
