import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
public class CalculatorTest {

    // Test addition
    @Test
    public void Add_OneAndTwo_ExpectedThree(){
        var expected = 3;
        var calculator = new Calculator();
        var actual = calculator.add(1,2);
        assertEquals(expected, actual);

    }
    // Test substraction
    @Test
    public void Sub_TwoAndOne_ExpectedThree(){
        var expected = 1;
        var calculator = new Calculator();
        var actual = calculator.sub(2,1);
        assertEquals(expected, actual);
    }
    // Test division
    @Test
    public void Div_TwoAndOne_ExpectedTwo(){
        var expected = 2;
        var calculator = new Calculator();
        var actual = calculator.div(2,1);
        assertEquals(expected, actual);
    }
    // Test multiplication
    @Test
    public void Mult_TwoAndOne_ExpectTwo(){
        var expected = 2;
        var calculator = new Calculator();
        var actual = calculator.mult(2,1);
    }
}
