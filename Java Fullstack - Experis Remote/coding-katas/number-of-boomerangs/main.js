/**
 * Number of Boomerangs
A boomerang is a V-shaped sequence that is either upright or upside down. Specifically, a boomerang can be defined as: sub-array of length 3, with the first and last digits being the same and the middle digit being different.
 */

function countBoomerangs(array) {
  let count = 0;
  const boomerangs = findAllTriples(array, isBoomerang);
  console.log(boomerangs);
  count = boomerangs.length;
  return count;
}

function findAllTriples(array, predicate = (triple) => true) {
  const length = array.length;
  const triples = [];
  for (let i = 2; i < length; i++) {
    const triple = [array[i - 2], array[i - 1], array[i]];
    if (predicate(triple)) {
      triples.push(triple);
    }
  }
  return triples;
}

function isBoomerang(triple) {
  if (triple.length === 3) {
    [firstDigit, middleDigit, lastDigit] = triple;
    if (firstDigit === lastDigit) {
      if (firstDigit !== middleDigit) {
        return true;
      }
    }
  }
  return false;
}

console.log(countBoomerangs([9, 5, 9, 5, 1, 1, 1]));
console.log(countBoomerangs([5, 6, 6, 7, 6, 3, 9]));
console.log(countBoomerangs([4, 4, 4, 9, 9, 9, 9]));
console.log(countBoomerangs([1, 7, 1, 7, 1, 7, 1]));
