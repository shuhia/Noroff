const testCases = new Map();
testCases.set(
  [
    ["X", "O", "X"],

    ["O", "X", "O"],

    ["O", "X", "X"],
  ],
  "X"
);
testCases.set(
  [
    ["O", "O", "O"],

    ["O", "X", "X"],

    ["E", "X", "X"],
  ],
  "O"
);
testCases.set(
  [
    ["X", "X", "O"],

    ["O", "O", "X"],

    ["X", "X", "O"],
  ],
  "Draw"
);

const DRAW = "DRAW";
const playerOne = "X";
const playerTwo = "O";
const players = [playerOne, playerTwo];
testCases.forEach((expectedResult, testValue) => {
  const result = ticTacToe(testValue);
  console.log("Passed:", result === expectedResult);
  console.log(
    `testValue: ${testValue}, Expected:${expectedResult}, Result: ${result}`
  );
});
/**
 * Determines the winner of TicTacToe
 * @param {[Array]} - Two dimensional array 3x3
 * @return {string} - returns x if x is winner, o if o is winner or draw if no winner
 */
function ticTacToe(matrix = [[]]) {
  const rows = matrix.map((row) => row);
  const cols = matrix[0].map((_, colIndex) =>
    matrix.map((row) => row[colIndex])
  );
  const leftDiag = matrix.reduce(
    (diag, row, index) => [...diag, row[index]],
    []
  );
  const rightDiag = matrix.reduce(
    (diag, row, index) => [...diag, row[row.length - 1 - index]],
    []
  );
  const lines = [...rows, ...cols, leftDiag, rightDiag];
  for (line of lines) {
    for (player of players) {
      if (line.every((mark) => player === mark)) return player;
    }
  }
  return "Draw";
}
function getElement(x, y) {
  return matrix[y][x];
}
function getCol(x) {
  const col = [];
  for (let y = 0; matrix[0].length; y++) {
    col.push[getElement(x, y)];
  }
  return col;
}
