package com.company;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    @org.junit.jupiter.api.Test
    void isValidIP() {
        assertTrue(Main.isValidIP("1.2.3.4"));
        assertFalse(Main.isValidIP("1.2.3"));
        assertFalse(Main.isValidIP("1.2.3.4.5"));
        assertTrue(Main.isValidIP("123.45.67.89"));
        assertFalse(Main.isValidIP("123.456.78.90"));
        assertFalse(Main.isValidIP("123.045.067.089"));
        assertFalse(Main.isValidIP("123.bas.067.089"));
    }
}