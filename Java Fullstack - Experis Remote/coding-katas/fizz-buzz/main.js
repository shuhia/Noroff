function fizzBuzz(n = 1) {
  if (n >= 1) {
    const FIZZ = "Fizz";
    const BUZZ = "Buzz";
    if (isMultipleOfFifteen()) {
      return FIZZ + BUZZ;
    } else if (isMultipleOfThree()) {
      return FIZZ;
    } else if (isMultipleOfFive()) {
      return BUZZ;
    } else {
      return n;
    }
  }

  function isMultipleOfFifteen() {
    return isMultipleOfFive() && isMultipleOfThree();
  }

  function isMultipleOfFive() {
    return n % 5 === 0;
  }

  function isMultipleOfThree() {
    return n % 3 === 0;
  }
}

const test = () => {
  for (let index = 0; index < 26; index++) {
    console.log("n = " + index + " result: " + fizzBuzz(index));
  }
};

test();
