import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Objects;
import java.util.Scanner;

class Interpreter {
    // Read input
    HashMap<String, Short> definitions = new HashMap<>(10000);
    Scanner scanner;

    public Interpreter(File file) throws FileNotFoundException {
        this.scanner = new Scanner(file);
    }
    public String run(){
        StringBuilder out = new StringBuilder();
        while(scanner.hasNext()){
            // readline
            var nextLine = scanner.nextLine();
            var inputs = nextLine.split(" ");
            // interpret line
            var command = inputs[0];
            var refOne = inputs[1];
            var refTwo = inputs[2];
            if(inputs.length < 4){
                define(refOne, refTwo);
            }
            else {
                // read additional input
                var refThree = inputs[3];
                var result = eval(refOne, refTwo, refThree);
                // print result
                out.append(result).append("\n");
            }
        }
        return out.toString();
    } public void define(String number, String key){
        definitions.put(key, Short.parseShort(number));
    }
    public String eval(String keyOne, String operator, String keyTwo){
        var numberOne = definitions.get(keyOne);
        var numberTwo = definitions.get(keyTwo);
        if(numberOne == null ||numberTwo == null){
            return "undefined";
        }
        switch(operator){
            case "<" -> {
                return String.valueOf(numberOne < numberTwo);
            }
            case ">" -> {
                return String.valueOf(numberOne > numberTwo);
            }
            case "=" -> {
                return String.valueOf(Objects.equals(numberOne, numberTwo));
            }
            default ->{
                return "invalid operator";
            }
        }
    }
}

public class Main {


    public static void main(String[] args) throws FileNotFoundException {
        var file = new File("src/main/resources/test");
        var interpreter = new Interpreter(file);
        var result = interpreter.run();
        System.out.println(result);



    }


}
