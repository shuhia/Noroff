/**
 * Find the length of a nested array.
 * The length is defined by the number of elements in the flattened array
 */

function getLength(array = []) {
  let length = 0;
  for (element of array) {
    if (Array.isArray(element)) {
      length += getLength(element);
    } else {
      length++;
    }
  }
  return length;
}

function verifyGetLength(array = []) {
  return array.flat(Infinity).length;
}

const testValues = [
  [1, [2, 3]],
  [1, [2, [3, 4]]],
  [1, [2, [3, [4, [5, 6]]]]],
  [1, [2], 1, [2], 1],
  ["103", [1], []],
];

testValues.forEach((value) =>
  console.log("value = " + value + " result: " + getLength(value))
);

function verifySolution(fn, testValues = this.testValues) {
  console.log(getLength(testValues) === verifyGetLength(testValues));
}
verifySolution();
