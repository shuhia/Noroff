
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Integer[] baseNodes = new Integer[]{0,1,0};
        FractalTree tree = new FractalTree(new ArrayList<Integer>(List.of(baseNodes)), 1);



    }

}
class FractalTree{
    Node root;
    public FractalTree(ArrayList<Integer> baseNodes, int k){
        root = new Node();
        // Construct fractal
        ArrayList<Node> nodes = new ArrayList<>(baseNodes.size());
        nodes.add(root);
        for(int i = 0; i<baseNodes.size(); i++){
            int parentNodeIndex = baseNodes.get(i);
            // get parent nod
            Node parentNode = nodes.get(parentNodeIndex);
            // Attach it to parent node
            if(parentNode.left == null){
                Node left = new Node();
                nodes.add(left);
                parentNode.left = left;
            }
            // Else attach it to left side
            else {
                Node right = new Node();
                nodes.add(right);
                parentNode.right = right;
            }
        }
    }

    public ArrayList<Node> getLeafNodes(){

    }

}
class Node {
    Node left = null;
    Node right = null;
    int label;
    public Node(){};
    public Node(int label){
        this.label = label;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }
}

class FractialTree{
    public FractialTree(){
    }
}