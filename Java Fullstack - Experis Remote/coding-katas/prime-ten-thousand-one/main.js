/**
 * Checks if a number is prime with Trail Division and lookup table
 * @param {number} number - number to checked
 * @returns {boolean} true if prime else false
 */

function isPrime(number = 2, previousPrimes = []) {
  let startNumber = number;

  // checks if number is divisable with previous primes
  for (const prime of previousPrimes) {
    if (number % prime === 0) return false;
    startNumber = prime;
  }

  // start at the next number after previous prime
  const upperLimit = Math.ceil(Math.sqrt(number));

  for (let integer = startNumber; integer < upperLimit; integer++) {
    if (number % integer === 0) return false;
  }
  return true;
}

/**
 * Finds the nth prime with lookup table and trail division
 * @param {number} n - nth prime number
 * @return {number} nth prime number
 */
function findNthPrime(n) {
  let number = 2;
  // lookup table
  let primes = [];
  while (primes.length < n) {
    // Checks if number is prime
    if (isPrime(number, primes)) {
      primes.push(number);
    }
    number++;
  }
  return primes.pop();
}

const primes = [2, 3, 5, 7, 11, 13];
primes.forEach((prime) => console.log(isPrime(prime)));

console.log(findNthPrime(10001));
