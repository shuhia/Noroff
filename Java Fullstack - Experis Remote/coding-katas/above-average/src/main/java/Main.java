import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        File inputFile = new File("src/main/resources/input-1.txt");
        File outputFile = new File("src/main/resources/output-1.txt");

        try (Scanner input = new Scanner(inputFile)) {
            // Number of test cases
            double nrTestCases = input.nextInt();
            while(input.hasNextInt()){
                int nrOfStudents = input.nextInt();
                double[] grades = new double[nrOfStudents];
                for (int i = 0; i < nrOfStudents; i++) {
                    grades[i] = input.nextInt();
                }

                // Percentage of students who have above average grade: StudentsAboveAverage/NrOfStudents
                final double averageGrade = Arrays.stream(grades).average().getAsDouble();
                double nrOfStudentsWithAboveAverageGrade =  Arrays.stream(grades).filter((grade)-> grade > averageGrade).count();
                double percentageWithAboveAverageGrade = nrOfStudentsWithAboveAverageGrade /nrOfStudents * 100;
                System.out.printf("%.3f %n", percentageWithAboveAverageGrade);
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


}
