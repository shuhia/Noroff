
import java.io.File;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        class Player {
            int v1;
            int v2;
            public Player(int v1, int v2) {
                this.v1 = v1;
                this.v2 = v2;
            }
            public Integer[] getSkills(){
                return new Integer[]{v1,v2};
            }
            public Integer[] add(Player player){
                return new Integer[]{this.v1 + player.v1, this.v2 + player.v2};
            }
        }
        ArrayList<Player> players = new ArrayList<Player>();
        File file = new File("src/main/resources/sample-input-1.txt");
        try( Scanner scanner = new Scanner(file)){
            int amountOfWorkers = scanner.nextInt();
            int v1;
            int v2;
            HashMap<Integer, Integer> map = new HashMap<>();
            while(scanner.hasNextInt()){
                v1 = scanner.nextInt();
                v2 = scanner.nextInt();
                players.add(new Player(v1,v2));
            }

            int averageSkillV1 =  players.stream().mapToInt((player) -> player.v1).sum()/(amountOfWorkers/2);
            int averageSkillV2 =  players.stream().mapToInt((player) -> player.v2).sum()/(amountOfWorkers/2);
            Integer[] averageSkill = new Integer[]{averageSkillV1, averageSkillV2};
            // Find match
            Set<Player> teamedUp = new HashSet<>();
            for (int i = 0; i < amountOfWorkers; i++) {
                // find match
                Player one = players.get(i);
                if(teamedUp.contains(one))continue;
                for (int j = 0; j <amountOfWorkers ; j++) {
                    Player two = players.get(i);
                    if(teamedUp.contains(two))continue;
                    // Match
                    Integer[] skillLevel = one.add(two);
                    if(isMatch(averageSkill, skillLevel)){
                        teamedUp.add(one);
                        teamedUp.add(two);
                        break;
                    };
                }

            }

            System.out.println(teamedUp.size() == (amountOfWorkers/2));





            // Create team with half amount of workers with same avg1 and avg2








        }
        // Sort the list
        // find the index where the average of all coworkers are equal.
        catch(Exception e){
            e.printStackTrace();
        }

    }

    private static boolean isMatch(Integer[] averageSkill, Integer[] skillLevel) {
        for (int k = 0; k < averageSkill.length; k++) {
           if(!Objects.equals(skillLevel[k], averageSkill[k])) return false;
        }
        return true;
    }


}
