function splitOnDoubleLetter(word) {
  if (typeof word === "string") {
    let previousChar = null;
    let newWord = "";
    // Mark each double letter with a space
    for (nextChar of word) {
      if (previousChar === nextChar) newWord += " ";
      newWord += nextChar;
      previousChar = nextChar;
    }
    // split at space
    const words = newWord.split(" ");
    if (words.length > 1) return words;
    else {
      return [];
    }
  }
}

const testValues = [
  "Letter",
  "Really",
  "Happy",
  "Shall",
  "Tool",
  "Mississippi",
  "Easy",
];

testValues.forEach((testValue) => {
  const result = splitOnDoubleLetter(testValue);
  console.log(result);
});
