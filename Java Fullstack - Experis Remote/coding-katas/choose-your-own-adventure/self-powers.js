function powerModulo(base, exponent, divisor) {
  let result = 1;
  for (let i = 0; i < exponent; i++) {
    result = ((result % divisor) * (base % divisor)) % divisor;
  }
  return result;
}

function findTenLastDigits(n) {
  let result = 0;
  const divisor = Math.pow(10, 10);
  for (let nextNumber = 1; nextNumber <= n; nextNumber++) {
    result =
      ((result % divisor) + powerModulo(nextNumber, nextNumber, divisor)) %
      divisor;
  }
  return result;
}

console.log(findTenLastDigits(1000));
