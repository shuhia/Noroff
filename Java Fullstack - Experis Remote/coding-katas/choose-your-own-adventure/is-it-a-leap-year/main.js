/**
 * This problem is based on algorithms, math, numbers and validation.
 * The purpose is to exercise in problemsolving
 * Problem difficulty: Expert
 * Link to problem: https://edabit.com/challenge/nNtYHuSDgnCq8gFvh
 */

/**
 * Algo from wikipedia: https://en.wikipedia.org/wiki/Leap_year
 *
 */

/**
 if (year is not divisible by 4) then (it is a common year)
 else if (year is not divisible by 100) then (it is a leap year)
 else if (year is not divisible by 400) then (it is a common year)
 else (it is a leap year)
 */

/**
 * Determines if given year is a leap year
 * @param {String} year
 * @return {Boolean} returns false or true
 */

function isLeapYear(year = 2000) {
  const divisors = [4, 100, 400];
  // Convert number to boolean
  const results = divisors.map((divisor) => !(year % divisor));
  // Convert boolean to a number that is either even or odd
  const result = results.reduce((sum, result) => sum + result, 0);
  // Convert number to 0 or 1 then convert number to odd or even
  // convert number to boolean.
  return !!(result % 2);
}

console.log(isLeapYear(2000));
console.log(isLeapYear(1521));
console.log(isLeapYear(1996));
console.log(isLeapYear(1800));
console.log(isLeapYear(2016));
