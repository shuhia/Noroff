function solution(n = 10) {
  return squareSum(n) - sumSquare(n);
}
function sumSquare(n) {
  return (n * (n + 1) * (2 * n + 1)) / 6;
}
function squareSum(n) {
  return n ** 4 / 4 + n ** 3 / 2 + n ** 2 / 4;
}
/**
 * 
 * @param {number}
 * @returns difference between squareSum and sumSquare
 */
function solutionSimplified(n = 10) {
  // n^4/4 + n^3/6 - n^2/4 - n/6
  return n ** 4 / 4 + n ** 3 / 6 - n ** 2 / 4 - n / 6;
}
console.log(solution(100));
console.log(solutionSimplified(100));
