const deck = [1, 2, 3, 4, 5, 6, 7, 8];

function shuffleCount(deck) {
  const originalDeck = [...deck];
  let shuffledDeck = shuffle([...deck]);
  let count = 1;
  while (!compareDeck(originalDeck, shuffledDeck)) {
    shuffledDeck = shuffle(shuffledDeck);
    count++;
  }
  return count;
}

function compareDeck(deckA, deckB) {
  for (let i = 0; i < deckA.length; i++) {
    if (deckA[i] !== deckB[i]) {
      return false;
    }
  }
  return true;
}

function splitDeck(deck) {
  const length = deck.length / 2;
  const deckA = deck.slice(0, length);
  const deckB = deck.slice(length);
  return [deckA, deckB];
}

function shuffle(deck) {
  const [firstDeck, secondDeck] = splitDeck(deck);

  const shuffledDeck = [];
  for (let index = 0; index < firstDeck.length; index++) {
    shuffledDeck.push(firstDeck[index]);
    shuffledDeck.push(secondDeck[index]);
  }
  return shuffledDeck;
}

function generateDeck(length) {
  return [...Array(length)].map((_, index) => index + 1);
}

function testShuffleCount(deckLength) {
  return shuffleCount(generateDeck(deckLength));
}
shuffleCount(deck);
console.log(testShuffleCount(8) === 3);
console.log(testShuffleCount(14) === 12);
console.log(testShuffleCount(52) === 8);
