function points(doublePointers, triplePointers) {
  const result = doublePointers * 2 + triplePointers * 3;
  console.log(result);
  return result;
}

function displayScore() {
  const doublePoints = document.getElementById("double-points").value;
  const triplePoints = document.getElementById("triple-points").value;
  const result = points(doublePoints, triplePoints);
  document.getElementById("score").value = result;
}

points(1, 1);
points(7, 5);
points(38, 8);
points(0, 1);
points(0, 0);
