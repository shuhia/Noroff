function correctTitle(sentence) {
  // The words and, the, of and in should be lowercase.
  // Convert all chars into lowercase

  const lowerCaseSentences = sentence
    .split(",")
    .map((sentence) => sentence.toLowerCase());
  const formattedSentences = [];
  for (lowerCaseSentence of lowerCaseSentences) {
    let lowerLetterWords = ["and", "the", "of", "in"];
    // Filter whitespace
    const words = lowerCaseSentence
      .split(" ")
      .filter((word) => word.trim() !== "");
    // Format the words
    const formattedWords = words.map((word) => {
      // if word should be lowercase
      if (lowerLetterWords.includes(word)) {
        return word;

        // else word should be uppercase
      } else {
        const firstChar = word.charAt(0);
        const newWord = word.replace(firstChar, firstChar.toUpperCase());
        return newWord;
      }
    });

    // join words with space and remove front space from comma.
    formattedSentences.push(formattedWords.join(" "));
  }

  return formattedSentences.join(", ");
}

const testSentences = [
  "jOn SnoW, kINg IN thE noRth.",
  "sansa stark,lady of winterfell.",
  "TYRION LANNISTER, HAND OF THE QUEEN.",
];
const correctSentences = [
  "Jon Snow, King in the North.",
  "Sansa Stark, Lady of Winterfell.",
  "Tyrion Lannister, Hand of the Queen.",
];

for (let i = 0; i < correctSentences.length; i++) {
  // YOUR SOLUTION
  const result = correctTitle(testSentences[i]);
  const expected = correctSentences[i];
  console.log(
    `case: ${i}\n result: ${result}\n Expected: ${expected} \n Passed: ${
      result === expected
    }`
  );
}
