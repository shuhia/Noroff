Game of Thrones: Character Titles
Write a function that takes a string and returns a string with the correct case for character titles in the Game of Thrones series.

The words and, the, of and in should be lowercase.
All other words should have the first character as uppercase and the rest lowercase.
All commas must always be followed by a single space.
All titles must end with a period.
Examples

correctTitle("jOn SnoW, kINg IN thE noRth") ➞ "Jon Snow, King in the North."

