package com.application;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

import static java.util.Collections.sort;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        File input = new File("src/com/application/input");
        Scanner scan = new Scanner(input);
        int testCount = scan.nextInt();
        for(int currentTest = 0; currentTest<testCount ; currentTest++ ){
            int numberCount = scan.nextInt();
            ArrayList<Integer> numbers = new ArrayList<>(numberCount);
            int sumOfMedians = 0;
            for(int i = 0; i<numberCount; i++){
                numbers.add(scan.nextInt());
                sumOfMedians += calculateMedian(numbers);
            }
            System.out.println(sumOfMedians);
        }
    }

    public static double calculateMedian(ArrayList<Integer> numbers){
        sort(numbers);
        int maxIndex = numbers.size()-1;
        // is even: 0,1,2,3
        // maps to: 0,1,2,3
        if(maxIndex%2 == 1){
            double middleIndex = ((double)maxIndex)/2;
            double leftOfMiddle = numbers.get((int)Math.floor(middleIndex));
            double rightOfMiddle =numbers.get((int)Math.ceil(middleIndex));
            return (leftOfMiddle + rightOfMiddle)/2;
        }
        else {
            return numbers.get(maxIndex/2);
        }
    }
}
