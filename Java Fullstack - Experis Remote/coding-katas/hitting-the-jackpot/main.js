modules.export = function testJackPot(array = ["@", "@", "@", "@", "@"]) {
  if (array.length === 4) {
    return array.every((value) => value === array[0]);
  } else return false;
};
