function caesarCipher(message = "middle-Outz", rotationFactor = 2) {
  let encryptedChars = "";
  for (const nextChar of message) {
    if (nextChar >= "a" && nextChar <= "z") {
      const rotatedChar = rotateLowerCaseCharBy(nextChar, rotationFactor);
      encryptedChars += rotatedChar;
    } else if (nextChar >= "A" && nextChar <= "Z") {
      const rotatedChar = rotateUpperCaseCharBy(nextChar, rotationFactor);
      encryptedChars += rotatedChar;
    } else {
      encryptedChars += nextChar;
    }
  }
  console.log(message + " => " + encryptedChars);
  return encryptedChars;
}

function rotateCharBy(char, rotationFactor) {}

function rotateUpperCaseCharBy(char, rotationFactor) {
  const firstCharCode = "A".charCodeAt(0);
  const lastCharCode = "Z".charCodeAt(0);

  const charCode = char.charCodeAt(0);
  let newCharCode = charCode;

  for (let rotation = 0; rotation < rotationFactor; rotation++) {
    newCharCode++;
    if (newCharCode > lastCharCode) {
      newCharCode = firstCharCode;
    }
  }
  const newChar = String.fromCharCode(newCharCode);
  return newChar;
}
function rotateLowerCaseCharBy(char, rotationFactor) {
  const firstCharCode = "a".charCodeAt(0);
  const lastCharCode = "z".charCodeAt(0);

  const charCode = char.charCodeAt(0);
  let newCharCode = charCode;

  for (let rotation = 0; rotation < rotationFactor; rotation++) {
    newCharCode++;
    if (newCharCode > lastCharCode) {
      newCharCode = firstCharCode;
    }
  }
  const newChar = String.fromCharCode(newCharCode);

  return newChar;
}

caesarCipher();
caesarCipher("Always-Look-on-the-Bright-Side-of-Life", 5);
caesarCipher("A friend in need is a friend indeed", 20);
