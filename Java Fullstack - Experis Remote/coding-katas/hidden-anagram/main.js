/**
 *
 * @param {*} sentence - a sentence that contains all letters that is needed to recreate message.
 * @param {*} message - a message to be embedded into the sentence
 */

function hiddenAnagram(sentence, message) {
  function normalize(chars) {
    return chars
      .toLowerCase()
      .split("")
      .filter((char) => char >= "a" && char <= "z")
      .join("");
  }
  // Normalize
  const formattedSentence = normalize(sentence);
  const formattedMessage = normalize(message);

  // find first char that matches
  let startIndex = formattedSentence.indexOf((char) => char === message[0]);
  while (startIndex < sentence.length - message.length) {
    const candidate = formattedSentence.substring(
      startIndex,
      startIndex + formattedMessage.length
    );
    if (candidate.length === formattedMessage.length) {
      const includesAllLetters = candidate.split("").reduce((result, char) => {
        return result && formattedMessage.includes(char);
      }, true);
      if (includesAllLetters) {
        console.log(candidate);
        return candidate;
      }
    }
    startIndex++;
  }
  return "";
}

console.log(
  hiddenAnagram("An old west action hero actor", "Clint Eastwood") ===
    "noldwestactio"
);
console.log(
  hiddenAnagram("Mr. Mojo Rising could be a song title", "Jim Morrison") ===
    "mrmojorisin"
);
console.log(hiddenAnagram("Banana? margaritas", "ANAGRAM") === "anamarg");
