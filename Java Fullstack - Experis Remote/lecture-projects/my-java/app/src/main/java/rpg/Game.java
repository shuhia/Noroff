package rpg;

import java.util.HashMap;
import java.util.Scanner;

public class Game {
  Scanner scanner;

  public Game() {}

  public void start() {
    scanner = new Scanner(System.in);
    run();
    scanner.close();
  }

  public void run() {
    // Create an example entity
    String name = ask("What is your name?");
    Entity player = new Entity(name, "player");
    Entity enemy = new Entity("Greg", "enemy");
    player.attack(enemy);
    enemy.attack(player);
  }

  public String ask(String message) {
    System.out.println(message);
    String line = scanner.nextLine();
    System.out.println("input: " + line);
    return line;
  }

  public void printAlts(String type) {}

  public void printResults() {}

  class Entity {
    protected HashMap<String, String> stats = new HashMap<>();

    public Entity(String name, String type) {
      this.put(STAT.NAME, name);
      this.put(STAT.TYPE, type);
      this.put(STAT.HEALTH, "100");
      this.put(STAT.DAMAGE, "5");
      System.out.println("created: " + this.stats.toString());
    }

    public void put(Enum key, String value) {
      this.put(key.toString(), value);
    }

    public void put(String key, String value) {
      this.stats.put(key, value);
      System.out.println("changed: " + this.stats());
    }

    public String get(Enum key) {
      return this.get(key.toString());
    }

    public String get(String key) {
      return this.stats.get(key);
    }

    public String stats() {
      return this.stats.toString();
    }

    public HashMap<String, String> attack(Entity target) {
      var entityAtk = this.get(STAT.DAMAGE);
      var targetHealth = target.get(STAT.HEALTH);
      var newHealth = Integer.parseInt(targetHealth) - Integer.parseInt(entityAtk);
      target.put(STAT.HEALTH, newHealth + "");
      return this.stats;
    }

    enum STAT {
      NAME,
      HEALTH,
      DAMAGE,
      TYPE,
    }
  }
}
